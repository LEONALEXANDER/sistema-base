<?php

use Illuminate\Support\Facades\Route;
use CloudCreativity\LaravelJsonApi\Facades\JsonApi;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api\V1\Auth')->prefix('api/v1')->middleware('json.api')->group(function () {
    Route::post('/login', 'LoginController');
    Route::post('/register', 'RegisterController');
    Route::post('/logout', 'LogoutController')->middleware('auth:api');
    Route::post('/password-forgot', 'ForgotPasswordController');
    Route::post('/password-reset', 'ResetPasswordController');
});



Route::namespace('Api\V1\configuracion')->prefix('api/v1')->middleware('json.api')->group(function () {
    //Route::resource('/users', 'AuthConroller');
    route::resource('/usuario', 'UsersController');
    Route::resource('/page', 'PageController');
    Route::get('/list_pages', 'PageControlle@listPages');
    Route::resource('/action', 'ActionController');
    Route::get('/list_actions', 'ActionController@listActions');
    Route::resource('/package', 'PackageController');
    Route::get('/list_packages', 'PackageController@listPackages');
    Route::resource('/profile', 'ProfileController');
    Route::get('/list_profiles', 'ProfileController@listProfiles');
});


JsonApi::register('v1')->middleware('auth:api')->routes(function ($api) {
    $api->get('me', 'Api\V1\MeController@readProfile');
    $api->patch('me', 'Api\V1\MeController@updateProfile');

    $api->resource('users');
});

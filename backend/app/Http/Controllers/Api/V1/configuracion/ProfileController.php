<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\ProfilePage;
use App\Models\ProfilePagePackage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $filter = json_decode($request->get('filter'));

        $getProfiles = Profile::where('name', 'LIKE', '%' . $filter->name . '%')
            ->orderBy('id', 'DESC')
            ->paginate($request->get('rows'));

        return response()->json($getProfiles, 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $newProfile = new Profile;
            $newProfile->id_user = Auth::id();
            $newProfile->id_user_update = Auth::id();
            $newProfile->name = $request->name;
            $newProfile->description = $request->description;
            $newProfile->save();

            foreach ($request->pages as $key => $value) {
                $newProfilePage = new ProfilePage;
                $newProfilePage->id_profile = $newProfile->id;
                $newProfilePage->id_page = $value['id'];
                $newProfilePage->save();

                foreach ($value['packages'] as $key2 => $value2) {
                    $newProfilePagePackage = new ProfilePagePackage;
                    $newProfilePagePackage->id_profile_page = $newProfilePage->id;
                    $newProfilePagePackage->id_package = $value2['id'];
                    $newProfilePagePackage->save();
                }
            }

            DB::commit();
            return response()->json('Perfil guardado con éxito', 201);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json('[Error - ProfileController@store]: ' . $th, 500); 
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $getProfile = Profile::with([
            'profilePages.page',
            'profilePages.profilePagesPackages.package'
        ])->find($id);
        return response()->json($getProfile, 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $updateProfile = Profile::find($id);
            $updateProfile->id_user_update = Auth::id();
            $updateProfile->name = $request->name;
            $updateProfile->description = $request->description;
            $updateProfile->save();

            $profilePages = $updateProfile->profilePages();
            foreach ($profilePages->get() as $key => $value) {
                $value->profilePagesPackages()->delete();
            }
            $profilePages->delete();

            foreach ($request->pages as $key => $value) {
                $newProfilePage = new ProfilePage;
                $newProfilePage->id_profile = $updateProfile->id;
                $newProfilePage->id_page = $value['id'];
                $newProfilePage->save();

                foreach ($value['selectedPackages'] as $key2 => $value2) {
                    $newProfilePagePackage = new ProfilePagePackage;
                    $newProfilePagePackage->id_profile_page = $newProfilePage->id;
                    $newProfilePagePackage->id_package = $value2['id'];
                    $newProfilePagePackage->save();
                }
            }

            DB::commit();
            return response()->json('Perfil editado con éxito', 200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json('[Error - ProfileController@update]: ' . $th, 500); 
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $updateProfile = Profile::find($id);
            $updateProfile->id_user_update = Auth::id();
            $updateProfile->active = $updateProfile->active == 1 ? 0 : 1;
            $updateProfile->save();

            DB::commit();
            return response()->json('Cambio de estatus con éxito', 200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json('[Error - ProfileController@destroy]: ' . $th, 500);
        }
    }

    public function listProfiles() {
        $list = Profile::where('active', 1)->get();
        return response()->json($list, 200);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $filter = json_decode($request->get('filter'));

        $getPackages = Package::where('name', 'LIKE', '%' . $filter->name . '%')
            ->orderBy('id', 'DESC')
            ->paginate($request->get('rows'));

        return response()->json($getPackages, 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $newPackage = new Package;
            $newPackage->id_user = Auth::id();
            $newPackage->id_user_update = Auth::id();
            $newPackage->name = $request->name;
            $newPackage->save();

            $actions = array_map(function($map) {
                $map = $map['id'];
                return $map;
            }, $request->actions);

            $newPackage->actions()->attach($actions);

            DB::commit();
            return response()->json('Paquete guardado con éxito', 201);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json('[Error - PackageController@store]: ' . $th, 500); 
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $getPackage = Package::with('actions')->find($id);
        return response()->json($getPackage, 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $updatePackage = Package::find($id);
            $updatePackage->id_user_update = Auth::id();
            $updatePackage->name = $request->name;
            $updatePackage->save();

            $actions = array_map(function($map) {
                $map = $map['id'];
                return $map;
            }, $request->actions);
            
            $updatePackage->actions()->detach();
            $updatePackage->actions()->attach($actions);

            DB::commit();
            return response()->json('Paquete editado con éxito', 200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json('[Error - PackageController@update]: ' . $th, 500); 
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $updatePackage = Package::find($id);
            $updatePackage->id_user_update = Auth::id();
            $updatePackage->active = $updatePackage->active == 1 ? 0 : 1;
            $updatePackage->save();

            DB::commit();
            return response()->json('Cambio de estatus con éxito', 200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json('[Error - PackageController@destroy]: ' . $th, 500);
        }
    }

    public function listPackages() {
        $list = Package::where('active', 1)->get();
        return response()->json($list, 200);
    }
}

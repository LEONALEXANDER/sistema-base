<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $filter = json_decode($request->get('filter'));

        $getPages = Page::with([
                'pageParent'
            ])
            ->where('name', 'LIKE', '%' . $filter->name . '%');
        
        if(isset($filter->page_parent) && $filter->page_parent) {
            $getPages = $getPages->whereHas('pageParent', function($query) use ($filter) {
                $query->where('name', 'LIKE', '%' . $filter->page_parent . '%');
            });
        }
            
        $getPages = $getPages->orderBy('id', 'DESC')->paginate($request->get('rows'));

        return response()->json($getPages, 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $newPage = new Page;
            $newPage->id_user = Auth::id();
            $newPage->id_user_update = Auth::id();
            $newPage->id_page_parent = $request->id_page_parent;
            $newPage->name = $request->name;
            $newPage->route = $request->route;
            $newPage->name_route = $request->name_route;
            $newPage->icon = $request->icon;
            $newPage->save();

            $packages = array_map(fn($map) => $map['id'], $request->packages);
            $newPage->packages()->attach($packages);

            DB::commit();
            return response()->json('Página guardada con éxito', 201);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json('[Error - PageController@store]: ' . $th, 500); 
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $getPage = Page::with('packages')->find($id);
        return response()->json($getPage, 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $updatePage = Page::find($id);
            $updatePage->id_user_update = Auth::id();
            $updatePage->id_page_parent = $request->id_page_parent;
            $updatePage->name = $request->name;
            $updatePage->route = $request->route;
            $updatePage->name_route = $request->name_route;
            $updatePage->icon = $request->icon;
            $updatePage->save();

            $packages = array_map(fn($map) => $map['id'], $request->packages);
            $updatePage->packages()->detach();
            $updatePage->packages()->attach($packages);

            DB::commit();
            return response()->json('Página editada con éxito', 200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json('[Error - PageController@update]: ' . $th, 500); 
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $updatePage = Page::find($id);
            $updatePage->id_user_update = Auth::id();
            $updatePage->active = $updatePage->active == 1 ? 0 : 1;
            $updatePage->save();

            DB::commit();
            return response()->json('Cambio de estatus con éxito', 200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json('[Error - PageController@destroy]: ' . $th, 500); 
        }
    }

    public function listPages() {
        $list = Page::with('packages')->where('active', 1)->get();
        return response()->json($list, 200);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;

    public function actions(){
        return $this->belongsToMany('App\Models\Action', 'package_actions', 'id_package', 'id_action')
            ->as('actions')
            ->withTimestamps();
    }

    public function pages(){
        return $this->belongsToMany('App\Models\Page', 'page_packages', 'id_package', 'id_page')
            ->as('pages')
            ->withTimestamps();
    }

    public function pages2(){
        return $this->belongsToMany('App\Models\Page', 'profile_page_packages', 'id_package', 'id_page')
            ->as('pages2')
            ->withTimestamps();
    }

    public function profiles(){
        return $this->belongsToMany('App\Models\Profile', 'profile_page_packages', 'id_package', 'id_profile')
            ->as('profiles')
            ->withTimestamps();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    public function pageParent() {
        return $this->belongsTo('App\Models\Page', 'id_page_parent');
    }

    public function packages(){
        return $this->belongsToMany('App\Models\Package', 'page_packages', 'id_page', 'id_package')
            ->as('packages')
            ->withTimestamps();
    }

    public function packages2(){
        return $this->belongsToMany('App\Models\Package', 'profile_page_packages', 'id_page', 'id_package')
            ->as('packages2')
            ->withTimestamps();
    }

    public function profiles(){
        return $this->belongsToMany('App\Models\Profile', 'profile_page_packages', 'id_page', 'id_profile')
            ->as('profiles')
            ->withTimestamps();
    }
}

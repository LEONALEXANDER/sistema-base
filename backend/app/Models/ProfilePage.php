<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfilePage extends Model
{
    use HasFactory;

    public function page() {
        return $this->belongsTo(Page::class, 'id_page');
    }

    public function profilePagesPackages(){
        return $this->hasMany(ProfilePagePackage::class, 'id_profile_page');
    }
}

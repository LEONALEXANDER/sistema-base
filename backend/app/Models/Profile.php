<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    public function profilePages(){
        return $this->hasMany(ProfilePage::class, 'id_profile');
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Action;
use App\Models\Package;
use App\Models\Page;
use App\Models\Profile;
use App\Models\ProfilePage;
use App\Models\ProfilePagePackage;


class UsuarioAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        try {
            DB::beginTransaction();
            

            $newUser = new User;
            $newUser->name = 'Raymond Blondell papito';
            $newUser->email = 'admin@jsonapi.com';
            $newUser->password = 'secret';
            $newUser->save();

            $arrayAcciones = ['Ver', 'Crear', 'Editar', 'Borrar'];

            foreach ($arrayAcciones as $value) {
                $newAction = new Action;
                $newAction->id_user = $newUser->id;
                $newAction->id_user_update = $newUser->id;
                $newAction->name = $value;
                $newAction->save();
            }

            $newPackage = new Package;
            $newPackage->id_user = $newUser->id;
            $newPackage->id_user_update = $newUser->id;
            $newPackage->name = 'Ver';
            $newPackage->save();

            $actions = [1];
            $newPackage->actions()->attach($actions);

            $newPackage = new Package;
            $newPackage->id_user = $newUser->id;
            $newPackage->id_user_update = $newUser->id;
            $newPackage->name = 'Ver, Crear';
            $newPackage->save();

            $actions = [1, 2];
            $newPackage->actions()->attach($actions);

            $newPackage = new Package;
            $newPackage->id_user = $newUser->id;
            $newPackage->id_user_update = $newUser->id;
            $newPackage->name = 'Ver, Crear, Editar';
            $newPackage->save();

            $actions = [1, 2, 3];
            $newPackage->actions()->attach($actions);

            $newPackage = new Package;
            $newPackage->id_user = $newUser->id;
            $newPackage->id_user_update = $newUser->id;
            $newPackage->name = 'Ver, Crear, Editar, Borrar';
            $newPackage->save();

            $actions = [1, 2, 3, 4];
            $newPackage->actions()->attach($actions);

            $newPage = new Page;
            $newPage->id_user = $newUser->id;
            $newPage->id_user_update = $newUser->id;
            $newPage->name = 'Configuración';
            $newPage->icon = 'fas,cogs';
            $newPage->save();

            $packages = [1];
            $newPage->packages()->attach($packages);

            $arrayPages = [
                [
                    'ruta' => '/users',
                    'nombreruta' => 'users',
                    'nombre' => 'Usuarios',
                    'paginapadre' => 1
                ],
                [
                    'ruta' => '/pages',
                    'nombreruta' => 'pages',
                    'nombre' => 'Páginas',
                    'paginapadre' => 1
                ],
                [
                    'ruta' => '/actions',
                    'nombreruta' => 'actions',
                    'nombre' => 'Acciones',
                    'paginapadre' => 1
                ],
                [
                    'ruta' => '/packages',
                    'nombreruta' => 'packages',
                    'nombre' => 'Paquetes',
                    'paginapadre' => 1
                ],
                [
                    'ruta' => '/profiles',
                    'nombreruta' => 'profiles',
                    'nombre' => 'Perfiles',
                    'paginapadre' => 1
                ],
            ];

            foreach ($arrayPages as $value) {
                $newPage = new Page;
                $newPage->id_user = $newUser->id;
                $newPage->id_user_update = $newUser->id;
                $newPage->name = $value['nombre'];
                $newPage->id_page_parent = 1;
                $newPage->route = $value['ruta'];
                $newPage->name_route = $value['nombreruta'];
                $newPage->save();

                $packages = [1, 2, 3, 4];
                $newPage->packages()->attach($packages);
            }

            $newProfile = new Profile;
            $newProfile->id_user = $newUser->id;
            $newProfile->id_user_update = $newUser->id;
            $newProfile->name = 'Administrador';
            $newProfile->description = 'Administrador';
            $newProfile->save();

            $packages = [1, 2, 3, 4];
            $pages = [1, 2, 3, 4, 5, 6];
            foreach ($pages as $value) {
                $newProfilePage = new ProfilePage;
                $newProfilePage->id_profile = $newProfile->id;
                $newProfilePage->id_page = $value;
                $newProfilePage->save();

                foreach ($packages as $value2) {
                    $newProfilePagePackage = new ProfilePagePackage;
                    $newProfilePagePackage->id_profile_page = $newProfilePage->id;
                    $newProfilePagePackage->id_package = $newProfilePage->id == 1 ? 1 : $value2;
                    $newProfilePagePackage->save();

                    if($newProfilePage->id == 1) break;
                }
            }

            $updateUser = User::find($newUser->id);
            $updateUser->id_profile = $newProfile->id;
            $updateUser->save();
            
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            dd($th);
        }
    }
}

import Vue from "vue"; // in Vue 2
import axios from "axios";
import VueAxios from "vue-axios";

Vue.use(VueAxios, axios);
Vue.axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL;

const ApiService = {
    get(resource, slug = "") {
        Vue.axios.defaults.headers.common["Authorization"] = window.localStorage.token;
        return Vue.axios.get(!slug ? `${resource}` : `${resource}/${slug}`);
    },

    post(resource, params) {
        Vue.axios.defaults.headers.common["Authorization"] = window.localStorage.token;
        Vue.axios.defaults.headers.common["Accept"] = "application/vnd.api+json";
        Vue.axios.defaults.headers.common["Content-Type"] = "application/vnd.api+json";
        return Vue.axios.post(`${resource}`, params);
    },

    update(module, id, params) {
        Vue.axios.defaults.headers.common["Authorization"] = window.localStorage.token;
        Vue.axios.defaults.headers.common["Accept"] = "application/vnd.api+json";
        Vue.axios.defaults.headers.common["Content-Type"] = "application/vnd.api+json";
        return Vue.axios.put(`${module}/${id}`, params);
    },

    del(module, id) {
        Vue.axios.defaults.headers.common["Authorization"] = window.localStorage.token;
        Vue.axios.defaults.headers.common["Accept"] = "application/vnd.api+json";
        Vue.axios.defaults.headers.common["Content-Type"] = "application/vnd.api+json";
        return Vue.axios.delete(`${module}/${id}`);
    },
};

export default ApiService;

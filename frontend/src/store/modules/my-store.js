import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default {
    state: {
        user: {},
        isAutheticated: false,
        menu: [],
    },

    getters: {
        getUser: state => state.user,
        getMenu: state => state.menu,
        isAutheticated: state => state.isAutheticated,
    },

    mutations: {
        setUser(state, payload) {
            console.log(payload);
            state.user = payload;
            state.isAutheticated = !!window.localStorage.token;
            state.menu = payload.profile.profile_pages;

            state.menu = state.menu.map(map => {
                map.hijos = [];
                map.estatusMenu = 1;
                return map;
            });

            state.menu.forEach(pag => {
                if (pag.page.id_page_parent) {
                    let buscarPadre = state.menu.find(
                        el => el.page.id == pag.page.id_page_parent
                    );

                    if (buscarPadre) {
                        buscarPadre.hijos.push(pag);
                        pag.estatusMenu = 0;
                    }
                }
            });

            state.menu = state.menu.filter(el => el.estatusMenu);

            console.log(state.menu);
        },
    },

    actions: {},
    modules: {},
};
